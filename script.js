/*Теоретичні питання

    1. Опишіть, як можна створити новий HTML тег на сторінці.

    Новий HTML тег на сторінці можна створити двома методами:
    - document.createElement(tag) - створює новий елемент з заданим тегом;
    - document.createTextNode(text) - створює новий текстовий вузол з заданим текстом.

    2. Опишіть, що означає перший параметр функції insertAdjacent HTML і опишіть можливі варіанти цього параметра.

    insertAdjacentHTML() розбирає вказаний текст як HTML або XML і встановлює отримані вузли (вузли) у дереві DOM у
    вказаній позиції. Ця функція не переписує наявні елементи, що передбачає додаткову серіалізацію, і тому працює
    швидше, ніж маніпуляції з innerHTML.
    DOMString- визначає позицію доданого елемента відносно елемента, викликаного методом. Потрібно відповідати одному з
    наступних значень (чутливо до реєстру):
    'beforebegin': до самого element (до відкриваючого тега).
    'afterbegin': відразу після відкриваючогося тега element(перед першим нащадком).
    'beforeend': відразу перед закриваючим тегом element(після останнього нащадка).
    'afterend': після елемента (після закриваючого тега).

    3. Як можна видалити елемент зі сторінки?

    Є два основних способи видалення елемента зі сторінки.
    Найбільш простим і зрозумілим є метод remove(). Все, що вам потрібно зробити - це звернутися до елемента і
    викликати метод remove().
    Оскільки метод remove() відноситься до нового стандарту, він зовсім не підтримується в Internet Explorer (до 11
    версії включно), Opera Mini (вона взагалі мало, що підтримує з нових стандартів), а більш прогресивних Chrome &
    Firefox підтримка є з 23 версії , Opera - з 15, в Safari - з 7, тобто. приблизно з 2012 року.
    Ще одним методом видалення елементів зі сторінки є removeChild(). Він існує доволі давно, тому ним варто
    користуватися, де потрібна підтримка старих браузерів. Однак із ним варто бути уважним, так як removeChild() можна
    тільки у його батька .

 */

function createList(array, parent = document.body) {
    const ul = document.createElement('ul');
    body.appendChild(ul);
    array.forEach(item => {
        const li = document.createElement('li');
        li.textContent = item;
        ul.appendChild(li);
    })
}
const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
displayList(array);